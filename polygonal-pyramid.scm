(define (scan-left f z xs)
  (cons z
        (if (pair? xs)
          (scan-left f (f z (car xs)) (cdr xs))
          xs)))

(define (merge-map merge f xs)
  (apply merge (map f xs)))

(define (^+^ v1 v2)
  (cons (+ (car v1) (car v2))
        (+ (cdr v1) (cdr v2))))

(define (*^ a v)
  (cons (* a (car v))
        (* a (cdr v))))

(define (polar-vec r θ)
  (*^ r (cons (cos θ) (sin θ))))

(define (lerp a v1 v2)
  (^+^ (*^ a v1) (*^ (- 1 a) v2)))

(define (gnomon sides step)
  (cond
    ((<= step 0)
     '())
    ((= step 1)
     '((0 . 0)))
    (#t
     (letrec*
       ((vertices
           (cdr (scan-left (lambda (pt i)
                             (^+^ pt
                                  (polar-vec (- step 1)
                                             (/ (* 2 pi i) sides))))
                           '(0 . 0)
                           (iota (- sides 1) 0))))
        
        (add-side-points (lambda (start end)
          (map (lambda (i) (lerp (/ i (- step 1)) start end))
               (iota (max (- step 2) 0) 1))))
        
        (add-points-between (lambda (xs)
          (if (and (pair? xs) (pair? (cdr xs)))
            (let ((x1 (car  xs))
                  (x2 (cadr xs))
                  (xs (cdr  xs)))
              (append (list x1)
                      (add-side-points x1 x2)
                      (add-points-between xs)))
            xs))))
       
       (add-points-between vertices)))))

(define (polygon sides side-length)
  (merge-map append (lambda (i) (gnomon sides i)) (iota side-length 1)))

(define (pyramid sides layers)
  (merge-map append
    (lambda (i)
      (map (lambda (pt) [(car pt) (cdr pt) (- layers i)])
           (polygon sides i)))
    (iota layers 1)))

(define* (draw-pyramid sides layers #:key (radius 0.25))
  (merge-map union (lambda (pt) (sphere radius pt)) (pyramid sides layers)))

(define* (pyramid-scene #:key (sides 1) (layers 1)
                              (radius 0.25)
                              (quality 11) (resolution 100))
  (let* ((extrema (cons '(0 . 0) (gnomon sides layers)))
         (xs      (map car extrema))
         (ys      (map cdr extrema))
         (padding (+ radius 0.5))
         (bounded (lambda (± bound) (lambda (cs)
                    (± (apply bound cs) padding))))
         (lower-bound (bounded - min))
         (upper-bound (bounded + max)))
    (set-bounds! [(lower-bound xs) (lower-bound ys) (- padding)]
                 [(upper-bound xs) (upper-bound ys) (+ layers padding)])
    (set-quality!    quality)
    (set-resolution! resolution)
    (draw-pyramid sides layers #:radius radius)))

(pyramid-scene #:sides 5 #:layers 4 #:radius 0.5)
