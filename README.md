# Polygonal Pyramids

![A pentagonal pyramid with 4 layers, made of of spheres that touch each other at tangent points in each pentagonal ring](Pentagonal pyramid with 4 layers.png)

To render the polygonal pyramids, open
[`polygonal-pyramid.scm`](polygonal-pyramid.scm) in
[libfive Studio](https://libfive.com/studio/).
