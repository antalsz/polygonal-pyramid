{-# LANGUAGE FlexibleContexts, TypeApplications, TypeFamilies #-}

module Graphics.PentagonalPyramid where

import Numeric.Natural

import Diagrams.Prelude hiding (pentagon)
import Diagrams.Backend.SVG.CmdLine

gnomon :: RealFloat n => Natural -> [Point V2 n]
gnomon 0 = []
gnomon 1 = [p2 (0,0)]
gnomon n =
  let side θ = (fromIntegral (n-1), θ) @@ r2PolarIso

      topLeftCorner  = side $ 180 + 36 @@ deg
      topRightCorner = side $ 0   - 36 @@ deg
      
      bottomLeftCorner  = topLeftCorner  ^+^ side (-72 @@ deg)
      bottomRightCorner = topRightCorner ^+^ side (-108 @@ deg)

      addSidePoints start end =
        let sep = fromIntegral n - 1
        in [lerp (i / sep) start end | i <- fromIntegral <$> [1..n-2]]

      addPointsBetween (x1:xs@(x2:_)) = x1 : addSidePoints x1 x2 ++ addPointsBetween xs
      addPointsBetween xs             = xs
  in P <$> addPointsBetween [topLeftCorner, bottomLeftCorner, bottomRightCorner, topRightCorner]

pentagon :: RealFloat n => Natural -> [Point V2 n]
pentagon = foldMap gnomon . enumFromTo 1

libfive :: Show n => [Point V2 n] -> Natural -> String
libfive pts z = unlines ["(sphere r [" ++ show x ++ " " ++ show y ++ " (layer " ++ show z ++ ")])" | P (V2 x y) <- pts]

draw
  :: (Monoid d, TrailLike d, Transformable d, HasOrigin d, Alignable d,
      RealFloat (N d), V d ~ V2) =>
     N d -> Natural -> d
draw r = foldMap (\p -> circle r & moveTo p) . pentagon

main :: IO ()
main = mainWith @(Diagram B) $ draw 0.25 4 & lw none & fc black
